<?php
require_once 'vendor/autoload.php';

$circulo = new Hijos\Circulo(2.5, 'cm');
$cuadrado = new Hijos\Cuadrado(7, 'dm');
$triangulo = new Hijos\Triangulo(12, 18, 'cm');
$rectangulo = new Hijos\Rectangulo(8, 5, 'm');

echo $circulo->calcularArea() . '<br>';
echo $cuadrado->calcularArea() . '<br>';
echo $triangulo->calcularArea() . '<br>';
echo $rectangulo->calcularArea() . '<br>';

?>