<?php
namespace Hijos;
use Padre\Poligono;

class Circulo extends Poligono {
  function __construct($radio, $medida) {
    $this->radio = $radio;
    $this->medida = $medida;
  }

  public function calcularArea() {
    return 'El área de un círculo de ' . $this->radio . ' ' .  $this->medida . ' de radio es ' .  3.1416 * pow($this->radio, 2) . ' ' .$this->medida .'2';
  }
}

?>