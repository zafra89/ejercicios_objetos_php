<?php
namespace Hijos;
use Padre\Poligono;

class Cuadrado extends Poligono {
  function __construct($lado, $medida) {
    $this->lado = $lado;
    $this->medida = $medida;
  }

  public function calcularArea() {
    return 'El área de un cuadrado de ' . $this->lado . ' ' . $this->medida .  ' de lado es ' . pow($this->lado, 2) . ' ' . $this->medida .'2';
  }
}

?>