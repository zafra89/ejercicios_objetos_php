<?php
namespace Hijos;
use Padre\Poligono;

class Rectangulo extends Poligono {
  function __construct($base, $altura, $medida) {
    $this->base = $base;
    $this->altura = $altura;
    $this->medida = $medida;
  }

  public function calcularArea() {
    return 'El área de un rectángulo de ' . $this->base . ' ' . $this->medida . ' de base y ' . $this->altura . ' ' . $this->medida . ' de altura es ' . $this->base * $this->altura . ' ' . $this->medida .'2';
  }
}

?>