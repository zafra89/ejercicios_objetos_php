<?php

class Fichero {

  public function __construct($nom, $destCopia, $destMover) {
    $this->nombreFichero = $nom;
    $this->destinoCopia = $destCopia;
    $this->destinoMover = $destMover;
  }

  public function borrar() {
    unlink($this->nombreFichero);
  }

  public function copiar() {
    copy($this->nombreFichero, $this->destinoCopia);
  }

  public function mover() {
    rename($this->nombreFichero, $this->destinoMover);
  }

  public function leer() {
    readfile($this->nombreFichero);
  }

  public function fechaModificacion() {
    filectime($this->nombreFichero);
  }
}


?>